'''
Created on Nov 9, 2012

@author: kosta
'''
import random
class ChessBoard(object):
    def __init__(self, state = [], parent=None):
        self.state = state
        self.parent = parent
        if not self.state:
            for i in range(0,8):
                row = []
                for j in range(0,8):
                    row.append(0)
                self.state.append(row)
            for i in range(0,8):
                while True:
                    random_row = random.randint(0,7)
                    random_col = random.randint(0,7)
                    
                    not_placable = False
                    for i in range(0,8):
                        if self.state[i][random_col] == "Q":
                            not_placable = True
                    if not_placable:
                        continue
                    
                    if self.state[random_row][random_col] == "Q":
                        continue
                    else:
                        self.state[random_row][random_col] = "Q"
                        break 

    def reshuffle(self):
        self.state = []
        for i in range(0,8):
            row = []
            for j in range(0,8):
                row.append(0)
            self.state.append(row)
        for i in range(0,8):
            while True:
                random_row = random.randint(0,7)
                random_col = random.randint(0,7)
                    
                not_placable = False
                for i in range(0,8):
                    if self.state[i][random_col] == "Q":
                        not_placable = True
                if not_placable:
                    continue
                    
                if self.state[random_row][random_col] == "Q":
                    continue
                else:
                    self.state[random_row][random_col] = "Q"
                    break 
    def __str__(self):
        string = "CHESSBOARD STATE \n"
        string += "  a  b  c  d  e  f  g  h \n"
        for i,row in enumerate(self.state):
            string += str(8-i)
            for tile in row:
                if tile == 0:
                    string += " - "
                else:
                    string += " Q "
            string += "\n"
        return string
    

    def to_list(self):
        myl = []
        for row in range(0,8):
            for col in range(0,8):
                if self.state[row][col] == "Q":
                    myl.append(1)
                else:
                    myl.append(self.state[row][col])
        return myl 
    
    def queen_count(self):
        count = 0
        for row in range(0,8):
            for col in range(0,8):
                if self.state[row][col] == "Q":
                    count += 1
        return count 
    
    def calculate_under_attack(self):
        diagonal_attacks = 0
        horiontal_attacks = 0
        for i in range(0, 8):
            for j in range(0,8):
                if self.state[i][j] == "Q":
                    #make sure its not attacking itsself
                    horiontal_attacks -= 2
                    for x in range(0,8):
                        if self.state[i][x] == "Q":
                            horiontal_attacks += 1
                        if self.state[x][j] == "Q":
                            horiontal_attacks += 1
                    """
                    Q-
                    -Q
                    """
                    x = i + 1
                    y = j + 1
                    while x < 8 and y < 8:
                        if self.state[x][y] == "Q":
                            diagonal_attacks += 1
                        x += 1
                        y += 1
                    
                    """
                    -Q
                    Q-
                    """
                    x =i + 1
                    y = j - 1
                    while x < 8 and y >= 0:
                        if self.state[x][y] == "Q":
                            diagonal_attacks +=1
                        x += 1
                        y -= 1                
                        
        return horiontal_attacks/2 + diagonal_attacks           
                    
            
                    