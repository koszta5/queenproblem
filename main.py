'''
Created on Nov 9, 2012

@author: kosta
'''
from solver import Solver
from threading import Thread
import sys
class Main():
    def __init__(self):
        pass
    
    def main(self):
        s = Solver()
        s.run()
        
    def homework(self):
        s = Solver(init_puzzle=[[0, 0, 0, 0, 'Q', 0, 0, 0], ['Q', 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 'Q', 0, 0], [0, 0, 0, 'Q', 0, 0, 0, 0], [0, 'Q', 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 'Q', 0], [0, 0, 'Q', 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 'Q']])
        s.run()
        
    def experiment(self, no_of_exp = 20):
        th1 = Experiment(no_of_expr=no_of_exp, no_of_conflicts=2)
        th2 = Experiment(no_of_expr=no_of_exp, no_of_conflicts=7)
        th1.start()
        th2.start()
            
            
class Experiment(Thread):
    
    def __init__(self, no_of_expr=20, no_of_conflicts=2):
        Thread.__init__(self)
        self.no_of_expr = no_of_expr
        self.solvers = []
        self.no_of_conflicts = no_of_conflicts
        
    def run(self):
        all_steps = 0
        for i in range(0, self.no_of_expr):
            s = Solver(no_of_conflicts=self.no_of_conflicts, silent=True)
            self.solvers.append(s)
            print "running solver"
            s.run()
        for s in self.solvers:
            all_steps += s.steps_len
        print "avarage step len for # of conflicts "+str(self.no_of_conflicts) + " is " + str(all_steps/self.no_of_expr)
            
            
    
if __name__ == "__main__":
    app = Main()
    print "What do you want to do (press numeric key)?"
    print "1 ---> run experiment (compare avarage number of steps for 2 & 5 initial conflicts)"
    print "2 ---> run randomly generated solver (generates random initial states and solves)"
    print "3 ---> run solver with given inital state (given by the PDF of homework assingment) *DEFAULT*"
    what_to_do = sys.stdin.readline()
    try:
        what_to_do = what_to_do.lstrip().rstrip()
    except AttributeError,e:
        pass
    if not what_to_do:
        what_to_do = 3
    what_to_do = int(what_to_do)
    if what_to_do == 1:
        app.experiment(no_of_exp=20)
    if what_to_do == 2:
        app.main()
    if what_to_do == 3:
        app.homework()
    sys.stdin.readline()
