'''
Created on Nov 9, 2012

@author: kosta
'''
import copy
from chessBoard import ChessBoard
import heapq
import time
from struct import pack

class PriorityQueue():
    def __init__(self):
        self.queue = []
        
    def append(self, x):
        heapq.heappush(self.queue, x)
        
    
    def pop(self):
        try:
            number,state = heapq.heappop(self.queue)
            return state
        except IndexError,e:
            print e
            print "This has no solution!!!"
            return None
            
class Solver(object):
    def __init__(self, init_puzzle = [], no_of_conflicts=None, silent=False):
        self.init_puzzle = init_puzzle
        self.no_of_conflicts_given = no_of_conflicts
        self.silent = silent
    
    def run(self):
        
        if not self.init_puzzle:
            if not self.no_of_conflicts_given:
                self.init_board = ChessBoard()
            else:
                self.init_board  = self.generate_board_with_given_conflict_nr(self.no_of_conflicts_given)
        else:
            self.init_board = ChessBoard(state=list(self.init_puzzle))      
        self.actual_board = copy.deepcopy(self.init_board)
        self.actual_attacks = self.actual_board.calculate_under_attack()
        self.queue= PriorityQueue()
        self.no_of_visited = 0
        self.no_of_generated = 0
        self.visited_states = []
        self.solve()
        
        
        
        
    def get_new_chess_board(self):
        packed_actual = pack(64*'B', *self.actual_board.to_list())
        if packed_actual not in self.visited_states:
            self.visited_states.append(packed_actual)
        for queen_row in range(0,8):
            for queen_col in range(0,8):
                if self.actual_board.state[queen_row][queen_col] == "Q":
                    for move_row in range(0,8):
                        for move_col in range(0,8):
                            if self.actual_board.state[move_row][move_col] != "Q":
                                possible_new_board = copy.deepcopy(self.actual_board)
                                possible_new_board.parent = self.actual_board
                                self.no_of_generated += 1
                                if (self.no_of_generated % 100) and not self.silent:
                                    print "no of generated -->" +str(self.no_of_generated)
                                possible_new_board.state[queen_row][queen_col] = 0
                                possible_new_board.state[move_row][move_col] = "Q"
                                packed_new = pack(64*'B', *possible_new_board.to_list())
                                if not packed_new in self.visited_states:
                                    self.no_of_visited += 1
                                    self.queue.append((possible_new_board.calculate_under_attack(), possible_new_board))
        self.actual_board = self.queue.pop()
        self.actual_attacks = self.actual_board.calculate_under_attack()
        
    def solve(self):
        while True:
            self.get_new_chess_board()
            if self.actual_attacks == 0:
                break
        if not self.silent:
            print "no of visited states " + str(self.no_of_visited)
            print "Printing steps" 
        steps = self.find_steps()
        if not self.silent:
            for step in reversed(steps):
                print step
        
        if self.actual_attacks == 0 and not self.silent:
            print "I FOUND a solution"
        else:
            if not self.silent:
                print "NO SOLUTION found"
                print "No of conflicts: "+ str(self.actual_board.calculate_under_attack())
        if not self.silent:
            print "Initial no of conflicts " +str(self.init_board.calculate_under_attack())
        self.steps_len = len(steps)
        if not self.silent:
            print "# of steps: "+ str(len(steps))
          
            
            
            
            
    def find_steps(self):
        steps = []
        steps.append(self.actual_board)
        state = self.actual_board
        while True:
            if state.parent:
                steps.append(state.parent)
                state = state.parent
            else:
                break
        return steps 
            
    def generate_board_with_given_conflict_nr(self, n):
        cb = ChessBoard()
        while True:
            if cb.calculate_under_attack() == n:
                return cb
            else:
                cb.reshuffle()
                                    
                                    
                                
                
        
        
        
        